// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    //Naive recursive method
    if n < 3 {
        return 1
    } else {
        return fibonacci_number(n-1) + fibonacci_number(n-2);
    }
}


fn main() {
    println!("{}", fibonacci_number(10));
}
