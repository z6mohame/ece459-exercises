use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let tx1 = mpsc::Sender::clone(&tx);

    let th0 = thread::spawn(move || {
        let val = String::from("Thread 0 sent this");
        tx.send(val).expect("Thread 0 failed to transmit");
        thread::sleep(Duration::from_millis(1000));
    });

    let th1 = thread::spawn(move || {
        let val = String::from("This was sent from thread 1");
        tx1.send(val).expect("Thread 1 failed to transmit");
        thread::sleep(Duration::from_millis(1000));
    });

    for received in rx {
        println!("Got: {}", received);
    }

    th0.join().expect("Thread 0 failed to join main");
    th1.join().expect("Thread 1 failed to join main");
    println!("All threads joined main");
}
