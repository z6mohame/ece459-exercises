// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum = 0;
    while number > 0 {
        number -= 1;
        sum += if (number % multiple1 == 0) || (number % multiple2 == 0) {number} else {0};
    }

    return sum;
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
