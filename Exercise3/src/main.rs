use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    //Create and start threads
    for i in 0..N {
        children.push(thread::spawn(move || {
            println!("Thread {} started", i);
        }));
    }

    //Join threads
    for child in children {
        child.join().expect("Thread join failure");
    }

    println!("All threads have ended an joined main");
}
